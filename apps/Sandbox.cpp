/*
 * Sandbox.cpp
 *
 *  Created on: 7 May 2020
 *      Author: Patrick Farwick
 */
#include "WhsCgPraktikum/Application.hpp"
#include "WhsCgPraktikum/Geometry.hpp"
#include "WhsCgPraktikum/IncludeFile.hpp"

#include <iostream>
#include <string>
#include <glm/glm.hpp>
#include <SDL2/SDL.h>

class Sandbox : public WHS::Application {
    private:
        glm::vec3 colour;
        WHS::Geometry geo;
    public:
        Sandbox(const std::string &title) : WHS::Application(title){
            std::cout << "Creating Sandbox Application" << '\n';
        }
        ~Sandbox() {
            std::cout << "Terminating Sandbox Application" << '\n';
        }

        void onEvent() override {
            switch (m_Event->type) {
                case SDL_KEYDOWN:
                case SDL_KEYUP:
                    switch (m_Event->key.keysym.sym) {
                        case SDLK_r:
                            colour.x = 255.f/255.f;
                            colour.y = 0.f;
                            colour.z = 0.f;
                            break;
                        case SDLK_g:
                            colour.x = 0.f;
                            colour.y = 255.f/255.f;
                            colour.z = 0.f;
                            break;
                        case SDLK_b:
                            colour.x = 0.f;
                            colour.y = 0.f;
                            colour.z = 255.f/255.f;
                            break;
                        case SDLK_w:
                            colour.x = 255.f/255.f;
                            colour.y = 255.f/255.f;
                            colour.z = 255.f/255.f;
                            break;
                        case SDLK_l:
                            colour.x = 0.f;
                            colour.y = 0.f;
                            colour.z = 0.f;
                            break;
                    }
                break;

                default:
                    break;
            }
        }

        void onUpdate() override {
            glClearColor(colour.x, colour.y, colour.z, 1.f);
            //geo.draw(glm::vec3(-0.9f,-0.9f,0.0f), glm::vec3(-0.5f,-0.5f,0.0f), glm::vec3(0.0f,-0.5f,0.0f));
            geo.drawDSA(glm::vec3(-0.9f,-0.9f,0.0f), glm::vec3(-0.5f,-0.5f,0.0f), glm::vec3(0.0f,-0.5f,0.0f));
            //geo.drawTriangle(glm::vec3(-0.9f,-0.9f,0.0f), glm::vec3(-0.5f,-0.5f,0.0f), glm::vec3(0.0f,-0.5f,0.0f));
        }
};

WHS::Application* WHS::CreateApplication() {
    const std::string &title{"Project"};
    return new Sandbox{title};
}