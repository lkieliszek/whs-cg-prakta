#ifndef WHSCGPRAKTIKUM_INCLUDEFILE_HPP
#define WHSCGPRAKTIKUM_INCLUDEFILE_HPP

// For use by Engine applications

#include "WhsCgPraktikum/Application.hpp"
#include <GL/glew.h>

// ---Entry Point---------------------
#include "WhsCgPraktikum/Main.hpp"
// -----------------------------------

#endif // WHSCGPRAKTIKUM_INCLUDEFILE_HPP