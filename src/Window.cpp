/*
 * Window.cpp
 *
 *  Created on: 7 May 2020
 *      Author: Patrick Farwick
 */
#include "WhsCgPraktikum/Window.hpp"
#include "WhsCgPraktikum/OpenGlDebugError.hpp"

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL_events.h>
#include <SDL_keycode.h>
#include <SDL_video.h>
#include <iostream>
#include <string>
#include <csignal>

namespace WHS {
    Window::Window(const std::string& title) {
        SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER);

        SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        createDebugContext();
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);

        m_Window = SDL_CreateWindow(title.c_str(), 
                    SDL_WINDOWPOS_CENTERED, 
                    SDL_WINDOWPOS_CENTERED,
                    1080, 
                    720, 
                    SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI
        );

        // Check that the window was successfully created
        if (m_Window == nullptr) {
            std::cerr << "Could not create window: " << SDL_GetError() << '\n';

            // x86 and x86_64 + Linux only.. 
            std::raise(SIGTRAP);
        }

        m_Context = SDL_GL_CreateContext(m_Window);
        if (!m_Context) {
            std::cerr << "Couldn't create context: " << SDL_GetError() << '\n';

            // x86 and x86_64 + Linux only.. 
            std::raise(SIGTRAP);
        }

        glewExperimental = GL_TRUE;
        GLenum err = glewInit();
        if (GLEW_OK != err) {
            std::cerr << "Error: " << glewGetErrorString(err);

            // x86 and x86_64 + Linux only.. 
            std::raise(SIGTRAP);
        }

        enableGLDebugging();

        // Check OpenGL properties
        std::cout << "Vendor: " << glGetString(GL_VENDOR) << '\n';
        std::cout << "Renderer: " << glGetString(GL_RENDERER) << '\n';
        std::cout << "Version: " << glGetString(GL_VERSION) << '\n'; 
        std::cout << "Extensions:" << '\n'; 
        int numberOfExtensions;
        glGetIntegerv(GL_NUM_EXTENSIONS, &numberOfExtensions);
        for(int i = 0; i < numberOfExtensions; ++i) {
            std::cout << '\t' << glGetStringi( GL_EXTENSIONS,i) << '\n'; 
        }

        SDL_ShowWindow(m_Window);
    }

    Window::~Window() {
        // Close and destroy the OpenGL context
        SDL_GL_DeleteContext(m_Context);
        // Close and destroy the window
        SDL_DestroyWindow(m_Window);
        // Clean up
        SDL_Quit();

        delete m_Event;
    }

    void Window::onEvent() {
        // ToDo: recommended to query events until poll event returns 0 (no more pending events).
        SDL_PollEvent(m_Event);
    }

    void Window::onUpdate() {
        SDL_GL_SwapWindow(m_Window);
    }

    SDL_Event* Window::getEvent() {
        return m_Event;
    }
}
