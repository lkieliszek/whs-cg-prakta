/*
 * Main.hpp
 *
 *  Created on: 7 May 2020
 *      Author: Patrick Farwick
 */
#ifndef WHSCGPRAKTIKUM_MAIN_HPP
#define WHSCGPRAKTIKUM_MAIN_HPP

#include "WhsCgPraktikum/Application.hpp"

extern WHS::Application* WHS::CreateApplication();

int main(int argc, char** argv) {
    auto app { WHS::CreateApplication() };
    app->run();
    delete app;
}

#endif // WHSCGPRAKTIKUM_MAIN_HPP