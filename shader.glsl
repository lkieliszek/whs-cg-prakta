#vertexSrc
#version 450 core

layout(location = 0) in vec4 position;

void main() {
    gl_Position = position;
}

#fragmentSrc
#version 450 core

layout(location = 0) out vec4 colour;

void main() {
    colour = vec4(0.443, 0.694, 0.153, 1.0);
}