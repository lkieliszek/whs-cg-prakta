/*
 * VertexBufferObject.cpp
 *
 *  Created on: 8 May 2020
 *      Author: Patrick Farwick
 */
 #include "WhsCgPraktikum/VertexBufferObject.hpp"

#include <GL/glew.h>

 namespace WHS {
    VertexBufferObject::VertexBufferObject(const float* vertices) {
        glCreateBuffers(1, &m_RendererID);
        glNamedBufferData(m_RendererID, sizeof(&vertices), vertices, GL_STATIC_DRAW);
    }

    VertexBufferObject::~VertexBufferObject() {
        glDeleteBuffers(1, &m_RendererID);
    }

    void VertexBufferObject::bind() const {
        glBindBuffer(GL_ARRAY_BUFFER, m_RendererID);
    }

    void VertexBufferObject::unbind() const {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
 }