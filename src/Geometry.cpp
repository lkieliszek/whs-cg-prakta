/*
 * Geometry.cpp
 *
 *  Created on: 8 May 2020
 *      Author: Patrick Farwick
 */
 #include "WhsCgPraktikum/Geometry.hpp"

#include <glm/glm.hpp>
#include <GL/glew.h>

namespace WHS {
    void Geometry::draw(glm::vec3 pos, glm::vec3 pos1, glm::vec3 pos2) {
        float vertices[9] = {
            pos.x, pos.y, pos.z,
            pos1.x, pos1.y, pos1.z,
            pos2.x, pos2.y, pos2.z
        };

        unsigned int m_VaoID;
        glGenVertexArrays(1, &m_VaoID);
        glBindVertexArray(m_VaoID);

        unsigned int m_VboID;
        glGenBuffers(1, &m_VboID);
        glBindBuffer(GL_ARRAY_BUFFER, m_VboID);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

        Shader shader = Shader("../../shader.glsl");
        shader.compileShader();
        shader.useShader();

        glDrawArrays(GL_TRIANGLES, 0, 3);

        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDeleteVertexArrays(1, &m_VaoID);
        glDeleteBuffers(1, &m_VboID);
    }

    void Geometry::drawDSA(glm::vec3 pos, glm::vec3 pos1, glm::vec3 pos2) {
        float vertices[9] = {
            pos.x, pos.y, pos.z,
            pos1.x, pos1.y, pos1.z,
            pos2.x, pos2.y, pos2.z
        };

        // Create a vertex array object
        unsigned int m_VaoID;
        glCreateVertexArrays(1, &m_VaoID);

        // enable index 0 of the attribute
        // this corrisponse to the layout qualifier 0 in the shader 
        glEnableVertexArrayAttrib(m_VaoID, 0);
        // specify the format of the data
        glVertexArrayAttribFormat(m_VaoID, 0, 3, GL_FLOAT, GL_FALSE, 0);

        unsigned int m_VboID;
        glCreateBuffers(1, &m_VboID);
        glNamedBufferData(m_VboID, sizeof(vertices), vertices, GL_STATIC_DRAW);

        // Specifies the binding-point
        glVertexArrayVertexBuffer(m_VaoID, 0, m_VboID, 0, sizeof(float) * 3);
        // Associate attrib 0 (first 0) with binding 0 (second 0).
        glVertexArrayAttribBinding(m_VaoID, 0, 0);

        Shader shader = Shader("../../shader.glsl");
        shader.compileShader();
        shader.useShader();

        glBindVertexArray(m_VaoID);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glBindVertexArray(0);
        glDeleteVertexArrays(1, &m_VaoID);
        glDeleteBuffers(1, &m_VboID);
    }

    // dead code
     void Geometry::drawTriangle(glm::vec3 pos0, glm::vec3 pos1, glm::vec3 pos2) {
        float vertices[9] = {
            pos0.x, pos0.y, pos0.z,
            pos1.x, pos1.y, pos1.z,
            pos2.x, pos2.y, pos2.z
        };

        // dead code
        VertexBufferObject vbo = VertexBufferObject(vertices);
        // dead code
        VertexArrayObject vao = VertexArrayObject();

        glVertexArrayVertexBuffer(vao.m_RendererID, 0, vbo.m_RendererID, 0, sizeof(vertices));
        vao.format(3);

        Shader shader = Shader("../../shader.glsl");
        shader.compileShader();

        // we start using the objects, now we have to bind them
        vbo.bind();
        vao.bind();
        shader.useShader();
        glDrawArrays(GL_TRIANGLES, 0, 3);
     }
}