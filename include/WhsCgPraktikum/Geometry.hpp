/*
 * Geometry.hpp
 *
 *  Created on: 8 May 2020
 *      Author: Patrick Farwick
 */
#ifndef WHSCGPRAKTIKUM_GEOMETRY_HPP
#define WHSCGPRAKTIKUM_GEOMETRY_HPP

#include "WhsCgPraktikum/Shader.hpp"
#include "WhsCgPraktikum/VertexArrayObject.hpp"
#include "WhsCgPraktikum/VertexBufferObject.hpp"

#include <glm/glm.hpp>

namespace WHS {
    class Geometry {
        public:
            void draw(glm::vec3 pos, glm::vec3 pos1, glm::vec3 pos2);
            void drawDSA(glm::vec3 pos, glm::vec3 pos1, glm::vec3 pos2);
            void drawTriangle(glm::vec3 pos0, glm::vec3 pos1, glm::vec3 pos2);
    };
}

#endif // WHSCGPRAKTIKUM_GEOMETRY_HPP