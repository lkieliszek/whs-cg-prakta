/*
 * Application.cpp
 *
 *  Created on: 7 May 2020
 *      Author: Patrick Farwick
 */
#include "WhsCgPraktikum/Application.hpp"

#include <GL/glew.h>
#include <iostream>
#include <string>

namespace WHS {
    Application::Application(const std::string& title) : m_Window{ title } {
        m_Event = m_Window.getEvent();
    }

    Application::~Application() { 
        delete m_Event;
    }

    void Application::run() {
        while (m_Running) {
            m_Window.onEvent();
            if (m_Event->type == SDL_QUIT) {
                m_Running = false;
                break;
            }
            onEvent();

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            onUpdate();
            m_Window.onUpdate();
        }
    }

    SDL_Event* Application::getEvent() {
        return m_Event;
    }
}