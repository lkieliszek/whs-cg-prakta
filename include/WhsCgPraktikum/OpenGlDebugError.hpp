/*
 * OpenGlDebugError.hpp
 *
 *  Created on: 11 May 2020
 *      Author: Patrick Farwick
 */
#ifndef WHSCGPRAKTIKUM_OPENGLDEBUGERROR_HPP
#define WHSCGPRAKTIKUM_OPENGLDEBUGERROR_HPP

#include <GL/glew.h>

 namespace WHS {
    void createDebugContext();
    void enableGLDebugging();
    void openGLLogMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);
 }

 #endif // WHSCGPRAKTIKUM_OPENGLDEBUGERROR_HPP