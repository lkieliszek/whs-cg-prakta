/*
 * Shader.cpp
 *
 *  Created on: 8 May 2020
 *      Author: Patrick Farwick
 */
#include "WhsCgPraktikum/Shader.hpp"

#include <alloca.h>
#include <string>
#include <fstream>
#include <iostream>
#include <csignal>
#include <sstream>
#include <GL/glew.h>

 namespace WHS {
    Shader::Shader(const std::string& filepath) {
        std::ifstream in(filepath, std::ios::in | std::ios::binary);
        if (!in.is_open()) {
            std::cerr << "Failed to read file." << '\n';

            // x86 and x86_64 + Linux only.. 
            std::raise(SIGTRAP);
        }
        bool isVertexSrc { true };
        std::string line;
        while (std::getline(in, line)) {
            if(line == "#vertexSrc") {
                isVertexSrc = true;
                continue;
            } else if(line == "#fragmentSrc") {
                isVertexSrc = false;
                continue;
            }
            
            if(isVertexSrc) {
                vertexSrc.append(line).append("\n");
            } else {
                fragmentSrc.append(line).append("\n");
            }
        }
    }

    Shader::~Shader() {
        glDeleteProgram(m_RendererID);
    }

    void Shader::compileShader() {
        m_RendererID = glCreateProgram();

        unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
        const char* vertex_Src_C_Str = vertexSrc.c_str();
        glShaderSource(vertexShader, 1, &vertex_Src_C_Str, nullptr);
        glCompileShader(vertexShader);

        // Error handling
        int vertexResult;
        glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &vertexResult);
        if(vertexResult == GL_FALSE) {
            int length;
            glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &length);
            char* message = (char*)alloca(length * sizeof(char));
            glGetShaderInfoLog(vertexShader, length, &length, message);
            
            std::cerr << "Error while compiling vertex shader!" << '\n' << message << '\n';

            glDeleteShader(vertexShader);
            return;
        }

        unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        const char* fragment_Src_C_Str = fragmentSrc.c_str();
        glShaderSource(fragmentShader, 1, &fragment_Src_C_Str, nullptr);
        glCompileShader(fragmentShader);

        // Error handling
        int fragmentResult;
        glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &fragmentResult);
        if(fragmentResult == GL_FALSE) {
            int length;
            glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &length);
            char* message = new char[length + 1];
            glGetShaderInfoLog(fragmentShader, length, &length, message);
            
            std::cerr << "Error while compiling vertex shader!" << '\n' << message << '\n';

            delete[] message;
            glDeleteShader(fragmentShader);
            return;
        }

        glAttachShader(m_RendererID, vertexShader);
        glAttachShader(m_RendererID, fragmentShader);
        glLinkProgram(m_RendererID);

        // I'm pretending the shaders successfully linked... not gonna bother to write another error handler

        glValidateProgram(m_RendererID);

        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
    }

    void Shader::useShader() {
        glUseProgram(m_RendererID);
    }
 }