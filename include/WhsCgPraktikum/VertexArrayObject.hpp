/*
 * VertexArrayObject.hpp
 *
 *  Created on: 8 May 2020
 *      Author: Patrick Farwick
 */
#ifndef WHSCGPRAKTIKUM_VERTEXARRAYOBJECT_HPP
#define WHSCGPRAKTIKUM_VERTEXARRAYOBJECT_HPP

#include <cstdint>

namespace WHS {
    class VertexArrayObject {
        public:
            // GLint is a 32bit integer, thus we can also use a normal 32bit integer
            uint32_t m_RendererID;
        public:
            VertexArrayObject();
            ~VertexArrayObject();
            void format(int size) const;
            void bind() const;
            void unbind() const;
    };
}

#endif // WHSCGPRAKTIKUM_VERTEXARRAYOBJECT_HPP