# WHS CG Praktikum

A project for the CG course of the WHS.

## Project dependencies

The requirements build are:

* [Conan Package Manager](https://conan.io/) -- Currently unused!
* [CMake 3.15 to CMake 3.17](https://cmake.org/)
* [Git](https://git-scm.com/)
* [SDL2](https://www.libsdl.org/)
* [GLEW](http://glew.sourceforge.net/)
* [GLM](https://glm.g-truc.net/0.9.9/index.html)
* [OpenGL](https://www.opengl.org/) 4.5 ready driver from your GPU manufacturer as we are using GL_ARB_direct_state_access

## Build project

You may need to add the `bincrafters` remote to conan.
```bash
conan remote add bincrafters "https://api.bintray.com/conan/bincrafters/public-conan"
```

Then:
```bash
mkdir build && cd build
conan profile new default --detect
```

###  Linux with Clang 

Create and then edit the default configuration to use Clang with C++11 and higher.
```bash
conan profile new default --detect
nano ~/.conan/profiles/default
```

Change the following lines (`clang --version` will tell you the clang version you've installed) or if you don't want to overwrite your default profile, create a new profile, add `include(default)` to it and then add the following lines to your new profile.
```json
compiler=clang
compiler.version=10
compiler.libcxx=libstdc++11

[env]
CC=/usr/bin/clang
CXX=/usr/bin/clang++
```
When creating a new profile, make sure to run `conan install --profile <profile_name> --build missing` instead of `conan install .. --build missing` in the next step. 

### Windows with ???

**Windows is currently not supported!**
Maybe someone can find the time to look into supporting it. ;)

### System independent
To build:

```bash
cd build
conan install .. --build missing
cd ..
cmake --build build
```

### Linux setup Part II

In your **top level folder**, run the following commands:

```bash
cmake .. -G "Unix Makefiles" -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_BUILD_TYPE=Debug
```

### Windows setup Part II

**Windows is currently not supported!**
Maybe someone can find the time to look into supporting it. ;)

### System independent setup Part II (Optional)

To test (`--target` can be written as `-t` in CMake 3.15+):
(to be added...)

```bash
cmake --build build --target test
```

To use an IDE, such as Xcode:

```bash
cmake -S . -B xbuild -GXcode
cmake --open xbuild
```
