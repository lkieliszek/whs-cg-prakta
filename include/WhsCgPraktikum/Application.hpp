/*
 * Application.hpp
 *
 *  Created on: 7 May 2020
 *      Author: Patrick Farwick
 */
#ifndef WHSCGPRAKTIKUM_APPLICATION_HPP
#define WHSCGPRAKTIKUM_APPLICATION_HPP

#include "WhsCgPraktikum/Window.hpp"

#include <string>

namespace WHS {
    class Application {
        private:
            bool m_Running { true };
            Window m_Window;
        protected:
            SDL_Event* m_Event;
        protected:
            virtual void onEvent() = 0;
            virtual void onUpdate() = 0;
        public:
            Application(const std::string& title);
            virtual ~Application();
            SDL_Event* getEvent();
            void run();
    };

    Application* CreateApplication();
}

#endif // WHSCGPRAKTIKUM_APPLICATION_HPP