/*
 * Shader.hpp
 *
 *  Created on: 8 May 2020
 *      Author: Patrick Farwick
 */
#ifndef WHSCGPRAKTIKUM_SHADER_HPP
#define WHSCGPRAKTIKUM_SHADER_HPP

#include <string>
#include <cstdint>
#include <vector>

namespace WHS {
    class Shader {
        private:
            uint32_t m_RendererID;
            std::string vertexSrc;
            std::string fragmentSrc;
        public:
            Shader(const std::string& filepath);
            ~Shader();
            void compileShader();
            void useShader();
    };
}

#endif // WHSCGPRAKTIKUM_SHADER_HPP