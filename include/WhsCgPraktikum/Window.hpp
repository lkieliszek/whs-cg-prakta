/*
 * Window.hpp
 *
 *  Created on: 7 May 2020
 *      Author: Patrick Farwick
 */
#ifndef WHSCGPRAKTIKUM_WINDOW_HPP
#define WHSCGPRAKTIKUM_WINDOW_HPP

#include <SDL2/SDL.h>
#include <string>

namespace WHS {
    class Window {
        private:
            SDL_Window* m_Window;
            SDL_Event* m_Event;
            SDL_GLContext m_Context;
        public:
            Window(const std::string& title);
            ~Window();
            void onEvent();
            void onUpdate();
            SDL_Event* getEvent();
    };
}

#endif // WHSCGPRAKTIKUM_WINDOW_HPP