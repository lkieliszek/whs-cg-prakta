/*
 * VertexBufferObject.hpp
 *
 *  Created on: 8 May 2020
 *      Author: Patrick Farwick
 */
#ifndef WHSCGPRAKTIKUM_VERTEXBUFFEROBJECT_HPP
#define WHSCGPRAKTIKUM_VERTEXBUFFEROBJECT_HPP

#include <cstdint>

namespace WHS {
    class VertexBufferObject {
        public:
            // GLint is a 32bit integer, thus we can also use a normal 32bit integer
            uint32_t m_RendererID;
        public:
            VertexBufferObject(const float* vertices);
            ~VertexBufferObject();
            void bind() const;
            void unbind() const;
    };
}

#endif // WHSCGPRAKTIKUM_VERTEXBUFFEROBJECT_HPP