/*
 * VertexArrayObject.cpp
 *
 *  Created on: 8 May 2020
 *      Author: Patrick Farwick
 */
 #include "WhsCgPraktikum/VertexArrayObject.hpp"

#include <GL/glew.h>

 namespace WHS {

    VertexArrayObject::VertexArrayObject() {
        glCreateVertexArrays(1, &m_RendererID);
        // https://stackoverflow.com/a/37972230
        // https://stackoverflow.com/a/55797849
    }

    VertexArrayObject::~VertexArrayObject() {
        glDeleteVertexArrays(1, &m_RendererID);
    }

    void VertexArrayObject::format(int size) const {
        glEnableVertexArrayAttrib(m_RendererID, 0);
        glVertexArrayAttribFormat(m_RendererID, 0, size, GL_FLOAT, GL_FALSE, 0);
        glVertexArrayAttribBinding(m_RendererID, 0, 0);
    }

    void VertexArrayObject::bind() const {
        glBindVertexArray(m_RendererID);
    }

    void VertexArrayObject::unbind() const {
        glBindVertexArray(0);
    }
 }