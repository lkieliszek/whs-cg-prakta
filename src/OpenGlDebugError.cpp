/*
 * OpenGlDebugError.cpp
 *
 *  Created on: 11 May 2020
 *      Author: Patrick Farwick
 */
 #include "WhsCgPraktikum/OpenGlDebugError.hpp"

#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <iostream>

namespace WHS {

    void createDebugContext() {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
    }

    void enableGLDebugging() {
        glDebugMessageCallback(openGLLogMessage, nullptr);
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    }

    void openGLLogMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam) {
        switch (severity) {
            case GL_DEBUG_SEVERITY_HIGH:
                std::cerr << "[OpenGL Debug HIGH] " << message << '\n';
                break;
            case GL_DEBUG_SEVERITY_MEDIUM:
                std::cout << "[OpenGL Debug MEDIUM] " << message << '\n';
                break;
            case GL_DEBUG_SEVERITY_LOW:
                std::cout << "[OpenGL Debug LOW] " << message << '\n';
                break;
            case GL_DEBUG_SEVERITY_NOTIFICATION:
                //std::cout << "[OpenGL Debug NOTIFICATION] " << message << '\n';
                break;
        }
    }

}